import io
import os
import json
import firebase_admin

from firebase_admin import credentials, db
from flask import Blueprint, request, jsonify
from google.cloud import vision
from google.protobuf.json_format import MessageToDict
from google.cloud.vision import types

client = vision.ImageAnnotatorClient()

detect_api = Blueprint('detect_api', __name__)

ref_information = db.reference('data/')


@detect_api.route('/checkstatus', methods=['GET'])
def checkstatus():

	if request.method == 'GET':

		body = []

		file_name = os.path.join(
		'tools/data/resources/maxresdefault.jpg')

		with io.open(file_name, 'rb') as image_file:
				content = image_file.read()

		image = vision.types.Image(content=content)

		response = client.document_text_detection(image=image)

		for page in response.full_text_annotation.pages:
			for block in page.blocks:
				print('\nBlock confidence: {}\n'.format(block.confidence))

				for paragraph in block.paragraphs:
					print('Paragraph confidence: {}'.format(
					paragraph.confidence))

					for word in paragraph.words:
						word_text = ''.join([
						symbol.text for symbol in word.symbols
						])
						print('Word text: {} (confidence: {})'.format(
							word_text, word.confidence))

						body.append(word_text)
								
		ref_information.push({
				'data': body
		})


	return jsonify(
			status='success',
			action='detect'
	)
