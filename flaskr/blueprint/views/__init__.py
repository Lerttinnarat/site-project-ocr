import os
import jwt
import firebase_admin

from firebase_admin import credentials
from firebase_admin import db
from flask import Blueprint

jwt_secret = os.getenv('JWT')

cred = credentials.Certificate(os.getenv('OCR'))
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://project-ocr-240815.firebaseio.com'
})

def detoken(token):
    return jwt.decode(token, jwt_secret, algorithms='HS256')
