import os
from flask import Flask 
from flask_cors import CORS

from .views.detect_api import detect_api

app = Flask(__name__)
CORS(app)

app.register_blueprint(detect_api, url_prefix='/detect_api')


@app.route("/")
def hello():

    return "Hello Wari!"
