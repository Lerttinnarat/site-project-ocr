FROM python:3.6.5-slim

RUN apt-get update

RUN mkdir /app

WORKDIR /app

COPY . /app

RUN pip install -r requirements.txt

ENV FLASK_ENV="docker"

CMD flask run --host=0.0.0.0 --port=5000

