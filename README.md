# site-project-ocr

Application Programming Interface for Site-Project-OCR

## Installation
```shell
virtualenv .venv -p /usr/bin/python3
source .venv/bin/activate
pip install -r requirements.txt
```

## Docker Build
```shell
docker-compose up -d --build
```

## Docker Push Repo
```shell
docker tag <IMAGE ID> <USERNAME>/<REPOSITORY>:<TAG> 
docker push <USERNAME>/<REPOSITORY>:<TAG> 
```